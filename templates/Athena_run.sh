#!/bin/zsh
#$ -l h_vmem=4G
#$ -e {logFile}
#$ -o {logFile}
#$ -j y

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export DQ2_LOCAL_SITE_ID=DESY-HH_SCRATCHDISK
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
export SITE_NAME=DESY-HH

cd {work_path}

if [ '{evgenJobOpts}' = '' ]; then 
	ln -s {jo} .
	ln -s {Athena}/MC15JobOptions .
fi

asetup AtlasDerivation 21.0.11.1, here
if [ ! -f {outputEVNTFile} ]; then
	Generate_tf.py --ecmEnergy {ecmEnergy} --runNumber {run_number} --randomSeed 2001 --firstEvent {firstEvent} --jobConfig {jo} --outputEVNTFile {outputEVNTFile} --inputGeneratorFile {inputGeneratorFile} {evgenJobOpts}
fi

if [ '{fmt[0]}' = 'DAOD' ]; then
	touch {outputFile}
	Reco_tf.py --inputEVNTFile={outputEVNTFile} --outputDAODFile=tmp.root --maxEvents=-1 --reductionConf={fmt[1]}
	rsync -a {fmt[0]}_{fmt[1]}.tmp.root {outputFile} --remove-source-files
elif [ '{fmt[0]}' = 'NTUP' ]; then
	Reco_tf.py --inputEVNTFile={outputEVNTFile} --outputNTUP_TRUTHFile={outputFile} --maxEvents=-1 --preExec='from D3PDMakerConfig.D3PDMakerFlags import jobproperties;jobproperties.D3PDMakerFlags.TruthWriteEverything=True'
fi
rc=$?
cd {root_path}
rm -r {work_path}
if [[ $rc != 0 ]]; then exit $rc; fi

rm {outputEVNTFile}