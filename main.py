#!/usr/bin/env python2.7
# PYTHON_ARGCOMPLETE_OK
import config as c
from utils import get_mg5version
import Sim
import Process as P

def set_config(**kwarg):
    for k, v in kwarg.items():
        setattr(c, k, v)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='MadgraphPythia Type-II 2HDM gg > H/A > tt~ (+ Interf) production.',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-i", "--iseed", type = int, default = c.iseed, help = "Random seed number")
    parser.add_argument("-e", "--ecm", type = int, help = "Central Mass Energy sqrt(s).")
    parser.add_argument("--param_card", default = "", help = "Instead of directly using the parameter set from run number/parameters, import a MG5 parameter card. Only used when generating event")
    parser.add_argument("-p", "--parton_level", nargs = '*', metavar = 'sim_opt', help = "Parton-level simulation. Powered by MG5_aMC@NLO{version}".format(version = 'v' + '.'.join(map(str, get_mg5version(c.MG5_path)))))
    parser.add_argument("-pv", "--parton_level_validation", nargs = '*', metavar = 'sim_opt', help = "Produce a ntuple that has all the neccessary parton-level information inside. The Matrix Element will be calculated as well. If <FFMT> is given, then the file matching will be done and stored into the ntuples.")
    parser.add_argument("-t", "--truth_level", nargs = '*', metavar = 'sim_opt', help = "Truth-level simulation. Powered by Pythia6")
    parser.add_argument("-tv", "--truth_level_validation", nargs = '*', metavar = 'sim_opt', help = "Produce a ntuple that has all the neccessary truth-level information. RWGT will be done at the procedure.")
    parser.add_argument("-c", "--cluster", default = c.cluster_type, help = "Batch system to use.")
    parser.add_argument("--ffmt", type = str, default = "DAOD_TRUTH1", help = "File format that will be produced or matched")
    parser.add_argument("--model", type = str, choices = ['2HDM', '2HDM+a'], help = 'Model to run.')
    # parser.add_argument("--file_format", choices = ["AOD", "NTUP_TRUTH"])
    subparsers = parser.add_subparsers()
    base_subparser = argparse.ArgumentParser(add_help=False)
    base_subparser.add_argument("-i", "--iseed", type = int, default = c.iseed, help = "Random seed number")
    base_subparser.add_argument("-e", "--ecm", type = int, help = "Central Mass Energy sqrt(s).", required = True)
    base_subparser.add_argument("--param_card", default = "", help = "Instead of directly using the parameter set from run number/parameters, import a MG5 parameter card. Only used when generating event")
    base_subparser.add_argument("-p", "--parton_level", nargs = '*', metavar = 'sim_opt', help = "Parton-level simulation. Powered by MG5_aMC@NLO{version}".format(version = 'v' + '.'.join(map(str, get_mg5version(c.MG5_path)))))
    base_subparser.add_argument("-pv", "--parton_level_validation", nargs = '*', metavar = 'sim_opt', help = "Produce a ntuple that has all the neccessary parton-level information inside. The Matrix Element will be calculated as well. If <FFMT> is given, then the file matching will be done and stored into the ntuples.")
    base_subparser.add_argument("-t", "--truth_level", nargs = '*', metavar = 'sim_opt', help = "Truth-level simulation. Powered by Pythia6")
    base_subparser.add_argument("-tv", "--truth_level_validation", nargs = '*', metavar = 'sim_opt', help = "Produce a ntuple that has all the neccessary truth-level information. RWGT will be done at the procedure.")
    base_subparser.add_argument("-c", "--cluster", default = c.cluster_type, help = "Batch system to use.")
    base_subparser.add_argument("--ffmt", type = str, default = "DAOD_TRUTH1", help = "File format that will be produced or matched")
    base_subparser.add_argument("--model", type = str, choices = ['2HDM', '2HDM+a'], required = True, help = 'Model to run.')
    # base_subparser.add_argument("--file_format", choices = ["AOD", "NTUP_TRUTH"], required=True)
    # Specify a process using physical parameters.
    parser_parameters = subparsers.add_parser("parameters",
                                              parents = [base_subparser],
                                              description="Specify a process using physical parameters.",
                                              help="Specify a process using physical parameters.",
                                              formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_parameters.add_argument("mediator", choices = ["H", "A"], help = "A possible Type-II 2HDM Mediator.")
    parser_parameters.add_argument("mass", help = "Mass of the type-II 2HDM Mediator.")
    parser_parameters.add_argument("tanb", help = "The ratio of the two vacuum expectation values.")
    parser_parameters.add_argument("signal_type", choices = ["S", "SI"], help = "Signal Only / Signal + Interf")
    parser_parameters.add_argument("slicer", default = "[:1000000:10000]", type = Sim.StrIterator, nargs = "?", help = "[start:stop:step_size]")
    # Specify a process using ATLAS official runNumbers.
    parser_runNumbers = subparsers.add_parser("run_number",
                                              parents = [base_subparser],
                                              description = "Specify a process using ATLAS official runNumbers.",
                                              help = "Specify a process using ATLAS official runNumbers 206768-206871.",
                                              formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_runNumbers.add_argument("run_number", help = "ATLAS official runNumbers 206768-206871.")
    parser_runNumbers.add_argument("slicer", default = "[:1000000:10000]", type = Sim.StrIterator, nargs = "?", help = "[start:stop:step_size]")
    args, additional_args = base_subparser.parse_known_args(namespace = parser.parse_args())

    if args.model == '2HDM':
        proc = P.ttbarResonanceProcess
    elif args.model == '2HDM+a':
        proc = P.THDMaTtRes

    if "run_number" in args:
        process = proc.from_run_number(args.run_number)
    else:
        process = proc(mediator = args.mediator, mass = float(args.mass), tanb = float(args.tanb), signal_type = args.signal_type)

    sim_chain = []
    if args.slicer == None:
        use_slicer = False
        args.slicer = c.slicer
    else:
        use_slicer = True
    kwargs = dict(process = process, slicer = args.slicer, iseed = args.iseed, ebeam1 = args.ecm/2, ebeam2 = args.ecm/2, use_cluster = args.cluster)
    if args.parton_level is not None:
        s = Sim.PartonLevelSim(**kwargs)
        for p in args.parton_level:
            exec('s.{}'.format(p))
        sim_chain.append(s.run)
    print args.parton_level_validation
    if args.parton_level_validation is not None:
        if args.parton_level is None:
            s = Sim.PartonLevelSim(**kwargs)
        for p in args.parton_level_validation:
            exec('s.{}'.format(p))
        sim_chain.append(s.validate)

    if args.truth_level is not None:
        s = Sim.TruthLevelSim(**kwargs)
        for p in args.truth_level:
            exec('s.{}'.format(p))
        sim_chain.append(s.run)

    if args.truth_level_validation is not None:
        if args.truth_level is None:
            s = Sim.TruthLevelSim(**kwargs)
        for p in args.truth_level_validation:
            exec('s.{}'.format(p))
        sim_chain.append(s.validate)

    if args.param_card == "r":
        process.write_param_card()

    for sim in sim_chain:
        sim(use_slicer, match = args.ffmt)
