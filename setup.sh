curdir=`pwd`

if [ ! $curdir -ef `dirname \"$0\"`  ]; then
    echo "ERROR: You must setup under the same directory of this script"
    return 1
fi

config_variable() {
	v=$(python -c "import config; print config.$1")
	echo $v
}

root_path=$(config_variable root_path)
MG5_path=$(config_variable MG5_path)
AtlasProject=$(config_variable AtlasProject)
AtlasVersion=$(config_variable AtlasVersion)

set_lhapdf() {
	# lhapdf_path=$(python -c "import config; ospath=config.os.path; print ospath.abspath(ospath.join(config.lhapdf_path, ospath.pardir, ospath.pardir)")
    export PATH=$MG5_path/HEPTools/lhapdf6/bin/:$PATH
    export LD_LIBRARY_PATH=$MG5_path/HEPTools/lhapdf6/lib/:$LD_LIBRARY_PATH
    export PYTHONPATH=$MG5_path/HEPTools/lhapdf6/lib/python2.7/site-packages/:$PYTHONPATH
}
set_lhapdf

mkdir -p $root_path/core/AnalysisTop/{build,source} && cd $root_path/core/AnalysisTop/build
acmSetup $AtlasProject,$AtlasVersion

unset set_lhapdf
unset config_variable
unset MG5_path
unset AtlasProject
unset AtlasVersion

cd $curdir