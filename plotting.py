import os
import config
import ROOT as R # temporary solution for the issue that rootpy canvas can't be initialised properly
R.TCanvas().Close()
import rootpy.ROOT as ROOT
# try:
#     ROOT.enableJSVis()
# except AttributeError:
#     pass
# STYLE = ""
STYLE = "DMWG"
if STYLE == "ATLAS":
    import AtlasStyle
elif STYLE == "DMWG":
    ROOT.gROOT.LoadMacro(os.path.join(config.root_path,'scripts/macros/DMWGStyle2017.cxx'))
    ROOT.SetDMWGStyle()
# from rootpy.tree import TreeChain
import os
import numpy as np
import xml.etree.ElementTree as ET
import Sim
import Process
import Algorithm
import copy


SIM = 1
DATA = 0
ODDS = 2
BKG = 3

logger = config.getLogger("2HDM_ttbarSI.plotting")
def resizer(func, size = None):
    if not callable(func):
        return np.resize(func, size)
    def func_wrapper(obj, size = size):
        size = size or obj.size
        return np.resize(func(obj), size)
    return func_wrapper

def ATLAS_LABEL(x, y, color = 0, suffix = "", text_align = 13, text_size = 36.0):
    canvas = ROOT.gPad or ROOT.Canvas()
    l = ROOT.TLatex(x, y, "#font[72]{ATLAS}" + ("#font[42]{ %s}" % suffix if suffix else ""))
    l.SetTextAlign(text_align)
    if text_size != None:
        l.SetTextSize(text_size)
    l.SetNDC()
    l.Draw()
    canvas.update()
    return l

def similarity(x, y):
    level = 0
    x = x.split('.')[::-1]
    y = y.split('.')[::-1]
    if len(x) > len(y):
        return -1
    for xi, yi in zip(x, y):
        if xi == yi:
            level += 1
        else:
            return level
    return level

class LegendEntry(object):
    def __init__(self, label=None,style=None):
        self.label = "" if label is None else label
        self.legendstyle = "" if style is None else style
    def GetTitle(self):
        return self.label

class NTupleStack(object):
    TEMPLATES = {'M()': dict(bins = np.arange(300, 1000+10, 10), xtitle = 'm^{parton}_{t#bar{t}} [GeV]', ytitle = 'Events/{:.0f}GeV'),
                 'truth.ttx.M()': dict(bins = np.array([0., 80., 160., 240., 320., 360., 400., 440., 500., 560., 600., 640., 680., 720., 760., 800., 860., 920., 1040., 1160., 1600.]),
                                       xtitle = 'm^{truth}_{t#bar{t}} [GeV]', ytitle = 'Events/{:.0f}GeV', bin_width = 40),
                 'W.M()': dict(bins = np.arange(50, 120+2, 2), xtitle = 'm_{W} [GeV]', ytitle = 'Events/{:.2f}GeV'),
                 'Pt()':dict(bins = np.arange(0, 400+16, 16), ytitle = 'Events/{:.2f}GeV'),
                 'Eta()':dict(bins = np.arange(-2.5, 2.5+0.1, 0.1), ytitle = 'Events/{:.2f}'),
                 'Phi():':dict(bins = np.arange(-np.pi, np.pi + 0.1, 0.1), ytitle = 'Events/{:.2f}'),
                 '': dict(bins = 10, ytitle = 'Events'),
                 'chi2':dict(bins = np.arange(-3, 10+0.5, 0.5), ytitle = 'Events/{:.2f}', xtitle = '\log_{10}{\chi^2}'),
                 'ev_hasRec':dict(ytitle = '%', xtitle = 'Cutflow')}
    def __init__(self, file = [], color = ["black", "red", "blue", "green"], drawstyle = ["H X0 P"], dtype = [DATA, SIM], sim = [], generator = "", sim_type = "TruthLevelSim", alg_type = 'SL', **kwargs):
        self.file = []
        for _f in file:
            if not isinstance(_f, ROOT.TFile.__bases__):
                f = ROOT.TChain('CollectionTree')
                f.Add(_f)
            else:
                f = _f
            self.file.append(f)
        self.file = np.asarray(self.file, dtype = object)
        print self.file
        Simulation = getattr(Sim, sim_type)
        if not sim:
            self.sim = [Process.ttbarResonanceProcess.from_run_number(os.path.basename(f.GetCurrentFile().GetName()).split(".")[3]) for f in self.file]
            self.sim = np.asarray([Simulation(s, **kwargs) if not s.reweighted_from else Simulation(s, **kwargs) for s in self.sim], dtype = object)
        else:
            # Not very efficient way. Can be improved later.
            sim = np.asarray(sim, dtype = object)
            #
            sim = np.reshape([Process.ttbarResonanceProcess.from_run_number(s) if not isinstance(s, (Sim.BasicSim, Process.ttbarResonanceProcess)) else s for s in sim.flat], sim.shape)
            self.sim = np.reshape([(Simulation(s, **kwargs) if not s.reweighted_from else Simulation(s, **kwargs)) if isinstance(s, Process.ttbarResonanceProcess) else s for s in sim.flat], sim.shape)
            if not self.file.flat:
                self.file = np.empty(np.shape(self.sim), dtype = object)
                for i, s in np.ndenumerate(self.sim):
                    if s.initialise(check_only = True) != 'DONE':
                        logger.error("Data does not exist. SKIP!")
                        continue
                    if not isinstance(s, Sim.BasicRW):
                        logger.info("Generated Sample")
                    else:
                        logger.info("RWGT Sample")
                    if getattr(type(s), "__name__", sim_type) == "PartonLevelSim":
                        dataset_ext = ("NTUP_LHE.root", )
                    elif getattr(type(s), "__name__", sim_type) == "TruthLevelSim":
                        dataset_ext = ("NTUP_TRUTH.root", )
                    # chain = TreeChain("CollectionTree", [f for f, exist in s.check_dataset(dataset_ext = dataset_ext, use_fname = True).iteritems() if exist])
                    chain = R.TChain("CollectionTree")
                    for f, exist in s.check_dataset(dataset_ext = dataset_ext, use_fname = True).iteritems():
                        if exist: chain.Add(f)
                    self.file[i] = chain
        # self.file = resizer(self.file, np.shape(self.sim))
        self._dtype = dtype
        if alg_type:
            self.status = getattr(Algorithm, alg_type)().STATUS_CODE
            self.TEMPLATES['ev_hasRec']['bins'] = np.arange(0, len(self.status)+1, 1)
        self._drawstyle = drawstyle
        self._color = color
        self._fillstyle = kwargs.pop('fillstyle', [(3017 if STYLE == "DMWG" else 3001) if d == DATA else 'hollow' for d in self._dtype])
        self._legendstyle = "LP"
        self.lumi = 20300
        if generator:
            self.generator = generator
        else:
            self.generator = [s.generator for s in self.sim.flat]
        self._title = kwargs.setdefault("title", self.generator)
        self.total_weight = []
        for f in self.file.flat:
            htemp = ROOT.Hist(1, 1, 2)
            #f.draw('1', 'weight[0]', hist = htemp)
            f.Draw('1>>{}'.format(htemp.name), 'weight[0]')
            self.total_weight.append(htemp.Integral())
            htemp.delete()
            ROOT.gPad.Delete()
        self.total_weight = np.reshape(self.total_weight, self.size)
        self.xsection = np.reshape([s.gen_xsection()['total'] if not isinstance(s, Sim.BasicRW) else (s.PreSim.gen_xsection()['total'] * self.total_weight[i]/self.total_event[i]) for i, s in np.ndenumerate(self.sim)], self.size)
        self.weightgroup = []
        for s in self.sim.flat:
            if s.lhebanner.has_key('initrwgt'):
                try:
                    self.weightgroup.append({int(w.attrib.pop('id')): dict(w.attrib, text = w.text.strip()) for w in ET.fromstring(s.lhebanner['initrwgt'])})
                except ET.ParseError:
                    pass
            self.weightgroup.append({0: {'id': 0, "MUR": None, "MUF": None, "DYN_SCALE": None, "PDF": None, 'text': 'Nominal'}})
        self._hist = []
    @property
    @resizer
    def title(self):
        return self._title
    @title.setter
    def title(self, v):
        self._title = v
        if not self._hist:
            return
        for i, title in enumerate(self.title):
            h = self._hist[i]
            h.title = title
            if ROOT.gPad:
                p = ROOT.gPad.primitives.FindObject('TPave').primitives[1:][i]
                p.item.SetLabel(title)
    @property
    @resizer
    def legendstyle(self):
        return self._legendstyle
    @legendstyle.setter
    def legendstyle(self, v):
        self._legendstyle = v
    @property
    def weight_factor(self):
        return self.lumi * self.xsection / self.total_weight
    @property
    def total_event(self):
        return np.reshape(getattr(self, "_total_event", np.array([f.GetEntries() for f in self.file.flat])), self.size)
    @property
    def size(self):
        return self.file.shape
    @property
    @resizer
    def dtype(self):
        return self._dtype
    @property
    @resizer
    def fillstyle(self):
        return self._fillstyle
    @fillstyle.setter
    def fillstyle(self, v):
        self._fillstyle = v
        for h, fillstyle in zip(self._hist, self.fillstyle):
            h.fillstyle = fillstyle
    @property
    @resizer
    def drawstyle(self):
        return self._drawstyle
    @drawstyle.setter
    def drawstyle(self, v):
        self._drawstyle = v
        for h, drawstyle in zip(self._hist, self.drawstyle):
            h.drawstyle = drawstyle
    def hist_property(self, attr, *args):
        len_args = len(args)
        if len_args > 1:
            raise SyntaxError("Set multiple values to Hist attribute.")
        elif len_args == 0:
            ret = []
            for h in self._hist:
                _attr = getattr(h, attr)
                if callable(_attr):
                    _attr = _attr()
                ret.append(_attr)
            return ret
        for h, p in zip(self._hist, resizer(args[0], size = self.size).flat):
            _attr = getattr(h, attr)
            if callable(_attr):
                _attr(p)
            else:
                setattr(h, attr, p)
    @property
    @resizer
    def color(self):
        return self._color
    @color.setter
    def color(self, value):
        self._color = value
        for h, color in zip(self._hist, self.color):
            h.color = color
    def hist(self, varexp, selection = "", options = "", *args, **kwargs):
        _hist = []
        if type(varexp) == list and np.size(varexp) != np.size(self.sim):
            logger.warn('inputs are `hists` without the same size as `sim`!')
            logger.warn('the `hists` are returned directly!')
            for h in varexp:
                yield h
            self._hist = varexp
            return
        varexp = varexp if varexp != 'Cutflow' else 'ev_hasRec'
        if type(varexp) is str:
            kwargs = dict(self.TEMPLATES[max(self.TEMPLATES, key = lambda k: similarity(k, varexp))], **kwargs)
            bins = kwargs.pop("bins")
            xtitle = kwargs.pop('xtitle')
            ytitle = kwargs.pop('ytitle')
        else:
            kwargs = dict({}, **kwargs)
            xtitle = kwargs.pop('xtitle', self._hist[0].xaxis.title)
            ytitle = kwargs.pop('ytitle', self._hist[0].yaxis.title)
        i = 0
        resized_kwargs = {}
        for k, v in kwargs.iteritems():
            if k not in ('drawstyle', 'color', 'title', 'fillstyle', "unweight", "weight_id", 'bin_width', 'legendstyle'):
                resized_kwargs[k] = resizer(v, self.size)

        for (f, dtype, drawstyle, color, title, sim, fac, fillstyle, legendstyle) in zip(self.file.flat,
                                                                            self.dtype,
                                                                            kwargs.pop('drawstyle', None) or self.drawstyle,
                                                                            kwargs.pop('color', None) or self.color,
                                                                            resizer(kwargs.pop('title', None) or self.title, self.size),
                                                                            self.sim,
                                                                            self.weight_factor,
                                                                            self.fillstyle,
                                                                            self.legendstyle):
            kwargs.update(drawstyle = drawstyle, color = color, title = title, fillstyle = fillstyle, legendstyle = legendstyle)
            for k, v in resized_kwargs.iteritems():
                kwargs[k] = v[i]
            logger.debug(repr(kwargs))
            bin_width = kwargs.pop('bin_width', None)
            normalized = kwargs.pop('normalized', False)
            # Just for efficiency, if `varexp` is a string, refill the historgrams; if not, reuse the existing histograms.
            # >>>
            if type(varexp) is str: # refill the hists based on `varexp`
                if not kwargs.pop("unweight", False):
                    _selection ='({})*weight[{}]*{}'.format(selection or 1, kwargs.pop("weight_id", 0), fac)
                    logger.debug(repr(_selection))
                # h = f.draw(varexp, _selection, options, hist = ROOT.Hist(bins, *args, **kwargs))
                h = ROOT.Hist(bins, *args, **kwargs)
                f.Draw(varexp + " >> {}".format(h.name), _selection, options)
                if similarity('ev_hasRec', varexp):
                    w = [bi.value for bi in h[1:-1]]
                    w.append(w.pop(0))
                    h.delete()
                    h = ROOT.Hist(bins, *args, **kwargs)
                    l = len(w)
                    for bi in range(l):
                        h.Fill(self.status[(bi+1)%l], sum(w[bi:]))
            elif varexp == None: # return last drawn hists
                h = self._hist[i]
                h.drawstyle = drawstyle
                h.fillstyle = fillstyle
                h.color = color
            else: # return input hists
                h = varexp[i]
                h.drawstyle = drawstyle
                h.fillstyle = fillstyle
                h.color = color
            # <<<
            if bin_width:
                h.Scale(bin_width, 'width')
            if normalized:
                h.Scale(1./h.Integral())
            h.xaxis.title = xtitle
            h.yaxis.title = ytitle.format(bin_width or h[0].x.width)
            h.title = kwargs['title'].format(s = self.sim[i])
            yield h
            _hist.append(h)
            i += 1
        self._hist = _hist
    def comparison_plot(self, varexp, selection = "", options = "", legend_kwds = {}, *args, **kwargs):
        canvas = ROOT.gPad or ROOT.Canvas()
        same = "sames" if kwargs.pop('same', False) else ""
        make_legend = kwargs.pop('make_legend', True)
        atlas_label = kwargs.pop('atlas_label', True)
        hists = list(self.hist(varexp, selection, options, *args, **kwargs))
        overall_maximum = max(h.max() for h in hists)
        overall_minimum = min(h.min() for h in hists)
        overall_pad = abs(overall_maximum - overall_minimum)
        for h in hists:
            h.yaxis.set_range_user(overall_minimum - 0.05*overall_pad, overall_maximum + 0.05*overall_pad)
            h.Draw(same)
            same = 'sames' 

        if make_legend:
            legend = self.legend(hists, atlas_label = atlas_label, **legend_kwds)
            self._legend = legend
            canvas.draw()
            # if STYLE == "DMWG":
            #     canvas.UseCurrentStyle()
            #     canvas.update()
            return canvas,
        # if STYLE == "DMWG":
        #     canvas.UseCurrentStyle()
        #     canvas.update()
        return canvas, 

    def legend(self, hists, *args, **kwds):
        offset = kwds.pop('offset', (0,0,0,0))
        canvas = ROOT.gPad or ROOT.Canvas()
        sim = self.sim[0]
        # if STYLE != 'DMWG':
        #     kwds.setdefault('textsize', 20)
        # else:
        #     kwds.setdefault('textsize', 20)
        kwds = dict(dict(textfont=42, textsize=0.045, atlas_label = False, banners = ["#sqrt{{s}} = 8 TeV, 20.3 fb^{{-1}}"]), **kwds)
        kwds = copy.deepcopy(kwds)
        print kwds
        headers = []
        if kwds.pop('atlas_label'):
            suffix = kwds.pop('suffix', None)
            headers.append(((0, "#font[72]{ATLAS}" + ("#font[42]{ %s}" % suffix if suffix else ""), 'h'), {'TextSize': 0.07}))
        header = kwds.pop('header', [])
        _headers = []
        if type(header) is str:
            _headers.append(header)
        else:
            _headers.extend(header)

        _headers.extend(kwds.pop('headers', []))
        headers += [((0, h, 'h'), dict()) for h in [b.format((sim.ebeam1+sim.ebeam2)//1000) for b in kwds.pop('banners', [])]+[h.format(repr(sim.process)) for h in _headers]]
        print headers
        legend = ROOT.Legend([], *args, **dict(dict(entrysep = 0.01, entryheight=0.03, **kwds)))
        for h, attrs in headers:
            legend.AddEntry(*h)
            if attrs:
                for k, v in attrs.iteritems():
                    getattr(legend.primitives[-1][0], 'Set'+k)(v)
        for h in hists:
            legend.AddEntry(h)
        legend.x1 = (0.55 + offset[0])
        legend.x2 = (0.90 + offset[1])
        legend.y1 = (0.5 + offset[2])
        legend.y2 = (0.88 + offset[3])
        legend.Draw()  
        canvas.update()
        canvas.update()
        return legend
    def ratio_plot(self, varexp, selection = "", options = "", legend_kwds = {}, *args, **kwargs):
        canvas = ROOT.gPad or ROOT.Canvas()
        same = ''
        make_legend = kwargs.pop('make_legend', False)
        hists = list(self.hist(varexp, selection, options, *args, **kwargs))
        base = hists[self.dtype.tolist().index(DATA)].clone()
        for bin in base:
            bin.error = 0
        hists_ = []
        for i in range(len(hists)):
            hists_.append(hists[i]/base)
            hists_[i].draw(same)
            same = 'same' 
        hists_[0].yaxis.title = "RWGT/MC"
        hists_[0].yaxis.range_user = (0.8, 1.2)
        if make_legend:
            self.legend(hists_)
        #     if STYLE == "DMWG":
        #         canvas.UseCurrentStyle()
        #         canvas.update()
        # if STYLE == "DMWG":
        #     canvas.UseCurrentStyle()
        #     canvas.update()
        return canvas
    def combined_plot(self, varexp, selection = "", options = "", legend_kwds = {}, *args, **kwargs):
        canvas = ROOT.Canvas()
        canvas.Divide(1, 2)
        hists = list(self.hist(varexp, selection, options, *args, **kwargs))
        canvas.cd(1)
        hists_ = [h.Clone() for h in hists]
        self.comparison_plot(hists_)
        hists_[0].xaxis.set_label_size(0)
        hists_[0].xaxis.set_title_size(0)
        canvas.cd(2)
        self.ratio_plot([h.clone() for h in hists])
        # if STYLE == "DMWG":
        #     canvas.UseCurrentStyle()
        #     canvas.update()
        return canvas
