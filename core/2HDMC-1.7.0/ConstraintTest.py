import subprocess
import numpy as np
from multiprocessing import Pool
import matplotlib.pyplot as plt
import pandas as pd
import re
# parameters = {"m_H+": None, "sin(b-a)": None,  "m_12^2": None, "m_h": 125, "yukawas_type": 2}
parameters_conv = {"m_H+": "m_Hp", "sin(b-a)": "sba", "m12^2": "m122", "tan(beta)": "tan_beta"}
pattern = re.compile("Total width:\s+(\S+)\s+GeV")
def GetParameters(m_H, m_A, tan_beta, lambda_6 = 0., lambda_7 = 0., m_Hp = None, sba = 1., m122 = None, yukawas_type = 2, m_h = 125, m122_scale = 1., **odds):
    """
    
    Usage: ./CalcPhys mh mH mA mHp sin(beta-alpha) lambda_6 lambda_7 m_12^2 tan_beta yukawas_type output_filename

    """
    m122 = (m122 or pow(m_A,2) * tan_beta/(1+pow(tan_beta,2)))*m122_scale
    m_Hp = m_Hp or m_H
    pars = map(str, (m_h, m_H, m_A, m_Hp, sba, lambda_6, lambda_7, m122, tan_beta, yukawas_type))
    output = subprocess.check_output(["./CalcPhys"] + pars).splitlines()
    output_parameters = {}
    get_mediator = False
    while output:
        l = output.pop(-1)
        if 'Cannot open file "(null)" for writing' in l:
            break
        matcher = pattern.match(l)
        if get_mediator:
            mediator = l.lstrip("Decay table for ").strip()
            get_mediator = False
            output_parameters['width_' + mediator.replace("+", "p")] = float(w)
        elif matcher:
            w = matcher.group(1)
            get_mediator = True
    for l in output:
        if not l.startswith("*"):
            kv = l.split(":")
            if len(kv) == 2 and bool(kv[-1]):
                k = kv[0].strip()
                output_parameters[parameters_conv.get(k, k)] = float(kv[1])
    output_parameters['m122_scale'] = m122_scale
    return output_parameters


def submit_job():
    pool = Pool(processes=12)
    res = []
    print 'Submiting jobs'
    for m_A in np.linspace(500, 1000., 11):
        for tanb in np.linspace(0.4, 2, 10):
            pars = GetParameters(500, m_A, tanb)
            for m122_scale in np.logspace(-5, 2, 50):
            # pars['m122'] = m122
           # par_sets[m122] = GetParameters(**dict(pars, m122 = m122))
               res.append(pool.apply_async(GetParameters, tuple(), dict(pars, m122_scale = m122_scale)))
    pool.close()
    print "Submiting done."
    return pool, res
pool, res = submit_job()
RGBA = dict(Stability = (1,0,0), Perturbativity = (0,1,0), Unitarity = (0,0,1))
# def rgba(an_array):
#     """
#     This is a temporary solution for the rgba tuple issue in pandas
#     """
#     return RGBA[tuple(an_array)]
def get(pool = pool, res = res):
    print 'Waiting jobs to finish'
    par_sets = []
    pool.join()
    for r in res:
        g = r.get()
        par_sets.append(g)
    df = pd.DataFrame(par_sets)
    df.rename(columns = {'Tree-level unitarity': 'Unitarity'}, inplace = True)
    df['score'] = df['Perturbativity'] + df['Stability'] + df['Unitarity']
    df['color'] = map(tuple, df[['Stability','Perturbativity','Unitarity']].values)
    # df['color'] = map(rgba, df[['Stability','Perturbativity','Tree-level unitarity']].values)
    return df

def max_score(df = None, query = None):
    if df is None:
        df = get()
    if query is not None:
        df.query(query, inplace = True)
    return df.sort_values(['score', 'lambda_5'], ascending=False).groupby(['m_A','tan_beta'],as_index=False,sort=False).first().sort_values(['m_A','tan_beta'])

def plotter(plt_kwds = {}, annotate_kwds = {}, **kwds):
    df = max_score(**kwds)
    plt_kwds = dict(dict(c = df.color.values, edgecolor = 'gray', s = 100), **plt_kwds)
    ax = df.plot.scatter(x='m_A',y='tan_beta', **dict(dict(title = kwds.get('query', None)), **plt_kwds))
    diff_xy = annotate_kwds.pop('diff_xy', (10., 0.))
    xytext = annotate_kwds.get('xytext', None)
    for i, row in df.iterrows():
        xy = row[['m_A', 'tan_beta']]
        if xytext is None:
            annotate_kwds['xytext'] = xy + diff_xy
        ax.annotate('{r[lambda_5]:.1f}'.format(r = row), xy = xy, **dict(dict(ha = 'left', va = 'center'), **annotate_kwds))
    for k, v in RGBA.iteritems():
        plt_kwds['c'] = v
        ax.scatter([], [], label = k, **plt_kwds)
    ax.legend()
    return df