include ( "MC15JobOptions/Pythia/Pythia_Perugia2011C_Common.py" )
include ( "MC15JobOptions/Pythia/Pythia_Photos.py" )
include ( "MC15JobOptions/Pythia/Pythia_Tauola.py" )
include ( "MC15JobOptions/Filters/TTbarWToLeptonFilter.py" )

# Enable xAOD making:
# from RecFlags import rec
# rec.doWritexAOD = True
# rec.doTruth = True

evgenConfig.description = "AFII MadGraph(CT10) Pythia Perugia 2011c gg->A->ttbar+interference, singlelepton+dilepton, m(A)=750GeV, 2HDM Type2, tan(beta)=0.40, sin(beta-alpha)=1.0, 13TeV cme"
evgenConfig.generators += ["MadGraph"]
evgenConfig.keywords = ["ggAtt", "ttbar", "singlelepton", "dilepton" ]
# evgenConfig.inputfilecheck = "madgraph.206782.ggAtt"
evgenConfig.contact  = ["katharina.behr@cern.ch", "yu-heng.chen@cern.ch", "jike.wang@cern.ch"]
evgenConfig.minevents=5000

theApp.TopAlg += ["Pythia", "TTbarWToLeptonFilter"]#, "Pythia_Photos", "Pythia_Photos"]
Pythia = Algorithm("Pythia")
Pythia.PythiaCommand +=  [
   "pyinit user madgraph"
   ]

#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
TTbarWToLeptonFilter = Algorithm("TTbarWToLeptonFilter")
TTbarWToLeptonFilter.NumLeptons = -1
TTbarWToLeptonFilter.Ptcut = 0.


