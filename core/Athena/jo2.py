include("MC12JobOptions/Pythia8_Base_Fragment.py")
include("MC12JobOptions/Pythia8_MadGraph.py")
topAlg.Pythia8.Commands +=  [
   "pyinit user madgraph",
   # "PartonLevel:MPI = off"
   ]

include ( "MC12JobOptions/Pythia8_Photos.py" )
include ( "MC12JobOptions/Pythia8_Tauola_UNVALIDATED.py" )
include ( "MC12JobOptions/TTbarWToLeptonFilter.py" )

evgenConfig.description = "AFII MadGraph(CT10) Pythia Perugia 2011c gg->A->ttbar+interference, singlelepton+dilepton, m(A)=750GeV, 2HDM Type2, tan(beta)=0.50, sin(beta-alpha)=1.0, 8TeV cme"
evgenConfig.generators += ["MadGraph"]
evgenConfig.keywords = ["ggAtt", "ttbar", "singlelepton", "dilepton" ]
# evgenConfig.inputfilecheck = "madgraph.206782.ggAtt"
evgenConfig.contact  = ["madalina.stanescu.bellu@cern.ch"]
evgenConfig.minevents=5000
#--------------------------------------------------------------
# Event filter
#--------------------------------------------------------------
topAlg.TTbarWToLeptonFilter.NumLeptons = -1
topAlg.TTbarWToLeptonFilter.Ptcut = 0.


