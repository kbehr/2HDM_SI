import os, glob

def short_name(file_path):
    file_path = os.path.split(file_path)[1].split(".")
    return ".".join([file_path[3], filter(lambda s: s.startswith("_"), file_path)[0]])

def file_pairing(filelist1, filelist2):
    for f1 in filelist1:
        f2 = filter(lambda f2: short_name(f1) == short_name(f2), filelist2)
        if f2:
            yield f1, f2[0]

def get_file_list(pattern_or_list):
    if type(pattern_or_list) == str:
        pattern_or_list = glob.glob(os.path.realpath(pattern_or_list))
    pattern_or_list.sort()
    return pattern_or_list

has_rc = False
def rcSetup(folder):
    global has_rc
    if not has_rc:
    # pwd = os.getcwd()
    # os.chdir(folder)
    # os.system(". /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/rcSetup/00-04-16/rcSetup.sh Top,2.4.21")
    # os.putenv("ROOTCOREDIR", "/cvmfs/atlas.cern.ch/repo/sw/ASG/AnalysisTop/2.4.21/RootCore")
        import ROOT
        ROOT.gROOT.Macro('$ROOTCOREDIR/scripts/load_packages.C')
        has_rc = True
    # os.chdir(pwd)
    # pass
def get_mg5version(mg5_path):
    """Get the version of MG5
    
    For some reason, MadGraph author decide to change some variable naming.
    So it is neccessary to check the version.
    ___Why you do this to me, bro.___
    
    Parameters
    ----------
    mg5_path : [path]
        Path of Madgraph

    Returns
    -------
    [tuple]
        Version Number
    """

    with open(os.path.join(mg5_path, 'VERSION')) as version:
        for l in version.readlines():
            k, v = l.split('=')
            if 'version' in k:
                return tuple(int(d) for d in v.split('.'))