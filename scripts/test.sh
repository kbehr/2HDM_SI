#!/bin/zsh
#$ -l h_vmem=4G
#$ -e /nfs/dust/atlas/user/chenyh/public/2HDM_ttbarSI/err.log
#$ -o /nfs/dust/atlas/user/chenyh/public/2HDM_ttbarSI/log.log
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export DQ2_LOCAL_SITE_ID=DESY-HH_SCRATCHDISK
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
export SITE_NAME=DESY-HH
lsetup root
localSetupGcc "gcc491_x86_64_slc6"
source ~/test/rcSetup.sh
cd /nfs/dust/atlas/user/chenyh/public/2HDM_ttbarSI
./main.py run_number 206769 -p -t
