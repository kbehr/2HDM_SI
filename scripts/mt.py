import Sim

s = Sim.PartonLevelSim(206768, "[:100000:100000]", decay = False, ebeam1 = 4000, ebeam2 = 4000)
s.process.MT = 171.5
s.systematics = ""
s.output_path = s.output_path + "_MT1D"
s.initialise()