import plotting
import Sim, Process
normalized = True

def ttres_2HDMa_A_ma():
    cc=iter(["black","red","blue","green","pink"])
    def sim_list():
        # filler = iter(['3205','3225', '3245', '3252', '3254'][::4])
        ret = []
        fillstyle = []
        color = []
        for model in ["THDM_noBKGRemoval", "THDMaTtRes_FormFac"][-1:]:
            for ma, c in zip([100,225,350,475,600], ["black","red","blue","green","pink"])[1:]:
            # for mediator, f in zip(['a','Aa','A'], [next(filler), 'hollow','hollow'])[::2]:
                for mass in [600]:
                    for tanb in [0.4, 1, 2][:1]:
                        for signal_type in ['SI']:
                            for mediator, f in zip(['a','Aa','A'], [3017, 'hollow',3017])[::2]:
                            # for ma, c in zip([100,225,350,475,600], ["black","red","blue","green","pink"])[:4]:
                                for sinp in [0, 0.5, 2**-0.5][-1:]:
                                    # c = next(cc)
                                    if sinp == 0 and mediator == "a":
                                        continue
                                    print f, c
                                    fillstyle.append(f)
                                    color.append(c)
                                    kwds = dict(mediator=mediator,mass=mass,tanb=tanb,signal_type=signal_type)
                                    if "THDMa" in model:
                                        kwds.update(sinp=sinp,MXd=10,ma=ma)
                                    p=getattr(Process,model)(**kwds)
                                    s=Sim.PartonLevelSim(p, ebeam1 = 4000, ebeam2 = 4000, slicer = "[:250000:10000]", decay = False, systematics = False)
                                    # s.run()
                                    # s.validate(matrix_element = False, match = "")
                                    ret.append(s)
                                    print s.run_path
        return ret, fillstyle, color
    sim, fillstyle, color = sim_list()
    ntup=plotting.NTupleStack(sim=sim)
    ntup._fillstyle=fillstyle
    ntup._color = color
    ntup._drawstyle='HIST'
    ntup.legendstyle = 'L'
    legend_kwds = dict(header = ['m_{A}=600GeV, tan#beta=0.4, sin#theta=1/#sqrt{2}'], offset = (0,0,0.03,0.03))#
    # legend_kwds = dict(header = '600GeV A->t#bar{t}+Interf, sin#theta=1/#sqrt{2}')
    ntup.title = ["m_{{{{a}}}}={ma}GeV".format(med=s.process.mediator, tanb=s.process.tanb, time = '' if s.process.mediator=='a' else "", sinp = "{:.1f}".format(s.process.sinp) if s.process.sinp in [0,0.5] else "1/#sqrt{{2}}", ma = s.process.ma) for s in ntup.sim]
    # ntup.title = ["tan#beta={tanb:.1f}".format(med=s.process.mediator, tanb=s.process.tanb, time = '' if s.process.mediator=='a' else "", sinp = "{:.1f}".format(s.process.sinp) if s.process.sinp in [0,0.5] else "1/#sqrt{{2}}", ma = s.process.ma) for s in ntup.sim]

    c, = ntup.comparison_plot('ttx.M()', make_legend=False, legend_kwds = legend_kwds, atlas_label = False)
    hs = list(ntup.hist(None))
    ntup.legend((hs)[1::2], **legend_kwds)
    print legend_kwds
    hhs = [h.Clone() for h in hs[:2]]
    for h,t in zip(hhs,['A->t#bar{t}+Interf', 'a->t#bar{t}+Interf'][::-1]):
        h.color = 'k'
        h.legendstyle = 'F'
        h.title = t
    ntup.legend(hhs, header='')
    for i, h in enumerate(ntup.hist(None)):
        pass
    return c, ntup
        # h.legendstyle = 'f'
        # if ntup.sim[i].process.mediator == 'a':
        #     h*=4
    # c, l = ntup.comparison_plot(None, legend_kwds = legend_kwds)

def ttres_2HDMa_sinp(med = 'A', force = False):
    cc=iter(["black","red","blue","green","pink"][1:])
    def sim_list():
        ret = []
        color = []
        ma = 100
        mediators = ['a','Aa','A'][-1:] if med == 'A' else ['H']
        for model in ["THDM_noBKGRemoval", "THDMaTtRes_FormFac"][-1:]:
            # for ma, c in zip([100,225,350,475,600], ["black","red","blue","green","pink"])[1:]:
                for mass in [600]:
                    for tanb in [0.4, 1, 2][:1]:
                        for signal_type in ['SI']:
                            for mediator in mediators:
                            # for ma, c in zip([100,225,350,475,600], ["black","red","blue","green","pink"])[:4]:
                                for sinp in [0, 0.5, 2**-0.5]:
                                    c = next(cc)
                                    if sinp == 0 and mediator == "a":
                                        continue
                                    print c
                                    color.append(c)
                                    kwds = dict(mediator=mediator,mass=mass,tanb=tanb,signal_type=signal_type)
                                    if "THDMa" in model:
                                        kwds.update(sinp=sinp,MXd=10,ma=ma)
                                    p=getattr(Process,model)(**kwds)
                                    s=Sim.PartonLevelSim(p, ebeam1 = 4000, ebeam2 = 4000, slicer = "[:250000:10000]", decay = False, systematics = False)
                                    if force:
                                        s.run()
                                        s.validate(matrix_element = False, match = "")
                                    ret.append(s)
                                    print s.run_path
        return ret, color
    sim, color = sim_list()
    ntup=plotting.NTupleStack(sim=sim)
    ntup.color = color
    ntup.drawstyle='HIST'
    ntup.legendstyle = 'L'
    # legend_kwds = dict(header = 'm_{A}=600GeV, tan#beta=0.4, sin#theta=1/#sqrt{2}', offset = (0,0,0.03,0.03))#
    legend_kwds = dict(headers = ['600GeV '+med+'->t#bar{{t}}+Interf', 'tan#beta=0.4, m_{{a}}=100GeV'], textfont=42, textsize=0.045, offset = (0,0,0,0))#, textsize=16
    # ntup.title = ["m_{{{{a}}}}={ma}GeV".format(med=s.process.mediator, tanb=s.process.tanb, time = '' if s.process.mediator=='a' else "", sinp = "{:.1f}".format(s.process.sinp) if s.process.sinp in [0,0.5] else "1/#sqrt{{2}}", ma = s.process.ma) for s in ntup.sim]
    ntup.title = ["sin#theta={sinp}".format(med=s.process.mediator, tanb=s.process.tanb, time = '' if s.process.mediator=='a' else "", sinp = "{:.1f}".format(s.process.sinp) if s.process.sinp in [0,0.5] else "1/#sqrt{{2}}", ma = s.process.ma) for s in ntup.sim]
    c, = ntup.comparison_plot('ttx.M()', legend_kwds = legend_kwds, atlas_label = False)
    print legend_kwds
    return c, ntup
        # h.legendstyle = 'f'
        # if ntup.sim[i].process.mediator == 'a':
        #     h*=4
    # c, l = ntup.comparison_plot(None, legend_kwds = legend_kwds)

def ttres_2HDMa_tanb(med = 'A', force = False):
    cc=iter(["black","red","blue","green","pink"][1:])
    def sim_list():
        ret = []
        color = []
        ma = 100
        mediators = ['a','Aa','A'][-1:] if med == 'A' else ['H']
        for model in ["THDM_noBKGRemoval", "THDMaTtRes_FormFac"][-1:]:
            # for ma, c in zip([100,225,350,475,600], ["black","red","blue","green","pink"])[1:]:
                for mass in [600]:
                    for tanb in [0.4, 1, 2]:
                        for signal_type in ['SI']:
                            for mediator in mediators:
                            # for ma, c in zip([100,225,350,475,600], ["black","red","blue","green","pink"])[:4]:
                                for sinp in [0, 0.5, 2**-0.5][-1:]:
                                    c = next(cc)
                                    if sinp == 0 and mediator == "a":
                                        continue
                                    print c
                                    color.append(c)
                                    kwds = dict(mediator=mediator,mass=mass,tanb=tanb,signal_type=signal_type)
                                    if "THDMa" in model:
                                        kwds.update(sinp=sinp,MXd=10,ma=ma)
                                    p=getattr(Process,model)(**kwds)
                                    s=Sim.PartonLevelSim(p, ebeam1 = 4000, ebeam2 = 4000, slicer = "[:250000:10000]", decay = False, systematics = False)
                                    if force:
                                        s.run()
                                        s.validate(matrix_element = False, match = "")
                                    ret.append(s)
                                    print s.run_path
        return ret, color
    sim, color = sim_list()
    ntup=plotting.NTupleStack(sim=sim)
    ntup.color = color
    ntup.drawstyle='HIST'
    ntup.legendstyle = 'L'
    # legend_kwds = dict(header = 'm_{A}=600GeV, tan#beta=0.4, sin#theta=1/#sqrt{2}', offset = (0,0,0.03,0.03))#
    legend_kwds = dict(headers = ['600GeV '+med+'->t#bar{{t}}+Interf', 'sin#theta=1/#sqrt{{2}}, m_{{a}}=100GeV'], textfont=42, textsize=0.045, offset = (0,0,0,0))
    # ntup.title = ["m_{{{{a}}}}={ma}GeV".format(med=s.process.mediator, tanb=s.process.tanb, time = '' if s.process.mediator=='a' else "", sinp = "{:.1f}".format(s.process.sinp) if s.process.sinp in [0,0.5] else "1/#sqrt{{2}}", ma = s.process.ma) for s in ntup.sim]
    ntup.title = ["tan#beta={tanb:.1f}".format(med=s.process.mediator, tanb=s.process.tanb, time = '' if s.process.mediator=='a' else "", sinp = "{:.1f}".format(s.process.sinp) if s.process.sinp in [0,0.5] else "1/#sqrt{{2}}", ma = s.process.ma) for s in ntup.sim]
    c, = ntup.comparison_plot('ttx.M()', legend_kwds = legend_kwds, atlas_label = False)
    print legend_kwds
    return c, ntup

def ttres_2HDMvs2HDMa(mediator='A'):
    sim = []
    mass = 600
    ma = 100
    signal_type = 'SI'
    sinp = 0
    tanb = 0.4
    for model in ["THDM_noBKGRemoval", "THDMaTtRes_FormFac"]:
        kwds = dict(mediator=mediator,mass=mass,tanb=tanb,signal_type=signal_type)
        if "THDMa" in model:
            kwds.update(sinp=sinp,MXd=10,ma=ma)
        p=getattr(Process,model)(**kwds)
        s=Sim.PartonLevelSim(p, ebeam1 = 4000, ebeam2 = 4000, slicer = "[:250000:10000]", decay = False, systematics = False)
        sim.append(s)
    ntup=plotting.NTupleStack(sim=sim)
    ntup.legendstyle = 'LP'
    # ntup.drawstyle='HIST'
    ntup.title = ['Type-II 2HDM', 'Type-II 2HDM+a, sin#theta=0']
    c, = ntup.comparison_plot('ttx.M()', atlas_label = False, legend_kwds = dict(header="600GeV {}->t#bar{{t}}+Interf, tan#beta=0.4".format(mediator)))
    return c, ntup
