import Process
import main
import glob, os
import run_numbers

for col in run_numbers.table[2::2]:
	num = col[2]
	p = Process.ttbarResonanceProcess.from_run_number(num)
	folder = os.path.join("output_pT2M2/" + str(p).replace("_v2","_v1"))
	if os.path.exists(folder):
		original_lhe = len(glob.glob(os.path.join(folder, "*.tar.gz")))
		cover_lhe = len(glob.glob("output_pT2M2_cover/*%d*/*.tar.gz" % num))
		main.c.iseed = 1000
	else:
		original_lhe = 0
		cover_lhe = len(glob.glob("output_pT2M2_cover/*%d*/*.tar.gz" % num))
	print original_lhe + cover_lhe, "iseed:", main.c.iseed
	while original_lhe + cover_lhe < 40:
		if original_lhe == 0:
			event_num = 1000000
		else:
			event_num = min(int((40 - original_lhe) * 1000000./original_lhe + 25e3) , 1000000)
		print event_num
		main.c.nevents = event_num
		main.generate_lhe(p, from_num = original_lhe + cover_lhe + 1)
		original_lhe = len(glob.glob(os.path.join(folder, "*.tar.gz")))
		cover_lhe = len(glob.glob("output_pT2M2_cover/*%d*/*.tar.gz" % num))
		main.c.iseed += 1