import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter

try:
	__IPYTHON__
	import IPython
	IPython.get_ipython().magic('matplotlib')
except NameError:
	pass

df_LO=pd.DataFrame(
 [(0.4, 120.6481, 0.9973174, 180.0776, 0.9982164),
 (0.68, 41.78946, 0.9962979, 62.35201, 0.9975532),
 (1.0, 19.37753, 0.9935174, 28.86613, 0.9963589),
 (2.0, 4.947328, 0.972844, 7.319296, 0.98237),
 (3.0, 2.393034, 0.8938866, 3.449651, 0.9263746),
 (4.0, 1.64554, 0.7312155, 2.239953, 0.8025008),
 (5.0, 1.457275, 0.5284361, 1.8378, 0.6259883),
 (6.0, 1.522338, 0.3512855, 1.786705, 0.4471457),
 (7.0, 1.739961, 0.2258074, 1.934352, 0.30344),
 (8.0, 2.059512, 0.1460594, 2.208517, 0.2034809),
 (9.0, 2.463044, 0.09649763, 2.581001, 0.1375723),
 (10.0, 2.939543, 0.06549288, 3.035323, 0.09475441)], columns = ('tanb', 'width_H', 'br_H_tt', 'width_A', 'br_A_tt'))

df = pd.read_csv('./core/width.dat', sep = '*')
df.dropna([0,1],how='all',inplace=True)
df.columns=map(str.strip, df.iloc[0])
df=df[1:].astype(float)
df.sort_values('tanb',inplace=True)

fig, axes = plt.subplots(1, 2, figsize = (8,3))
for ax, mediator in zip(axes, ['H', 'A']):
	df.plot(x='tanb',y=['width_{}'.format(mediator)],logy=True, ax = ax, c = 'k', legend = False)
	df_LO.plot(x='tanb',y=['width_{}'.format(mediator)], logy=True, ax = ax, marker = 'x', ls = 'none', c = 'k', legend = False)
	ax.set_xlabel('$\\tan{\\beta}$')
	ax.set_ylabel('$\mathrm{{\\Gamma({} \\rightarrow X)}}$'.format(mediator))
	ax.grid(which = 'major')
	ax.grid(which  = 'minor', ls = '--')
	plt.xticks([0.4] + range(1, 11),[0.4,'',2,'',4,'',6,'',8,'',10])
	# ax.set_title('$\\mathrm{m_H=m_A=m_{H^{\pm}}=600\,GeV}$, $\\mathrm{\\sin{\\mathit{\\theta}}=0.00}}$', fontsize = 'medium')
	ax2 = plt.twinx(ax)
	ax2.spines['right'].set_color('red')
	ax2.tick_params(axis='y', colors='red')
	ax2.set_ylim(0,1)
	ax2.set_ylabel('$Br(\\mathrm{{{} \\rightarrow t\\bar{{t}}}})$'.format(mediator), color = 'red')
	ax2.yaxis.set_major_formatter(FuncFormatter(lambda y, _: '{:.0%}'.format(y))) 
	df.plot(x='tanb',y=['br_{}_tt'.format(mediator)], ax = ax2, c = 'r', legend = False)
	df_LO.plot(x='tanb',y=['br_{}_tt'.format(mediator)], ax = ax2, marker = 'x', ls = 'none', c = 'r', legend = False)
	MG5, = ax.plot([],[],marker = 'x', ls = 'none', c = 'k', label = 'MadGraph5_aMC@NLO')
	THDMC, = ax.plot([], [], c = 'k', label = 'ATLAS 2HDM Recomm.')
	# THDMC.set_url('https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HiggsBSM2HDMRecommendations')
	ax.legend(handles = [MG5, THDMC], fontsize = 'xx-small', loc = 'upper right')
	ax.set_ylim(0.5, 200)
	ax.set_xlim(0.4,10)
fig.suptitle('$\\mathrm{m_H=m_A=m_{H^{\pm}}=600\,GeV}$, $\\mathrm{\\sin{(b-a)}=1.00}}$, $\\mathrm{\\sin{(\\mathit{\\theta})}=0.00}}$', fontsize = 'large')


plt.tight_layout(rect = [0, 0, 1, 0.95])
plt.show()
