import plotting

import Process as P
import Sim
from itertools import product
Sims = []
ma_color = dict(zip([100,200,300,400,500], ['k','r','g','b','purple']))
sinp_ls  = dict(zip([0.,0.35,2**(-0.5),1.], [8,29,23,34]))
try:
    for mediator in ['A']:#['A', 'H']:
        for tanb in [2]:#[0.4, 1, 2]:
            for ma in [100,200,300,400,500]:
                for sinp in [0.,0.35,2**(-0.5),1.]:
                    p=P.THDMaTtRes(mass=600,mediator=mediator,ma=ma,tanb=tanb,MXd=10.,sinp=float(sinp),signal_type='S')
                    s=Sim.PartonLevelSim(p,decay=False)
                    s.ebeam1=4000
                    s.ebeam2=4000
                    s.slicer="[0:10000:10000]"
                    s.dynamical_scale_choice=3                                                                                                                                     
                    s.output_path = "{{ecm}}TeV.{mediator}{{process.mass}}{tanb:03}_sinp{sinp:03}_ma{p.ma:03}_mxd{MXd:03}".format(mediator=mediator, tanb=int(p.tanb*100),sinp=int(p.sinp*100), MXd=int(p.MXd),p=p)
                    try:
                        s.initialise(check_only=True)
                        Sims.append(s)
                    except KeyError as e:
                        print e
            raise StopIteration()
except StopIteration:
    pass

ntup = plotting.NTupleStack(sim = [s for s in Sims], drawstyle = 'P HIST', fillstyle = 'none')
ntup._color = list(ma_color[s.process.ma] for s in ntup.sim)
fmt=dict(markerstyle=list(sinp_ls[s.process.sinp] for s in ntup.sim),
         normalized=True,
         title='({s.process.mh4}GeV, {s.process.sinp:.2f}, {s.process.width:.0f}GeV)',
         ytitle = 'Normalized')
def comparison_plot(varexp, *args, **kwargs):
    c,atlas = ntup.comparison_plot(varexp,*args,**dict(kwargs, **fmt))
    l = c.primitives.FindObject('TPave')
    l.SetFillStyle(1001)
    l.SetFillColorAlpha(plotting.ROOT.kCyan, 0.3)
    l.SetY1NDC(0.2)
    l.SetX1NDC(0.58)
    l.SetX2NDC(0.97)
    l.SetMargin(0.12)
    next(ntup.hist(None)).SetMaximum(0.05)
    l.Draw()
    c.Draw()
    return l