import os
import sys
import logging

msgfmt = '%(asctime)s %(levelname)-7s %(name)-20s %(message)s'
datefmt = '%H:%M:%S'


def getLogger(name = None, level = logging.DEBUG):
    logger = logging.getLogger(name)
    try:
        import coloredlogs
        coloredlogs.install(logger = logger, level = level, fmt = msgfmt, datefmt = datefmt)
    except ImportError:
        logging.basicConfig(format = msgfmt, datefmt = datefmt)
        logger.setLevel(level)
    return logger
logger = getLogger('2HDM_ttbarSI.Config')

try:
    try:
        __IPYTHON__
    except NameError:
        from IPython.core.ultratb import AutoFormattedTB
        sys.excepthook = AutoFormattedTB()
except ImportError:
    logger.warn('IPython is not installed. Colored Traceback will not be populated.')

root_path = os.path.dirname(os.path.realpath(__file__))
run_root = os.path.join(root_path, "Run")
MG5_path = os.path.join(root_path, "core", "MG5_aMC_v2_6_0")
sys.path.append(MG5_path)
mg5_run_mode = 2 # 1 for cluster 2 for multicore
cluster_type = "condor"
# gcc = "/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Gcc/gcc491_x86_64_slc6/slc6/gcc49/bin/gcc"
shell = "/bin/zsh"
slicer = "[:100000:10000]"
iseed = 21
TRUTH_FMT = "DAOD_TRUTH1"
AtlasProject = 'AnalysisTop'
AtlasVersion = '21.2.87'

# lhapdf_path = "/cvmfs/sft.cern.ch/lcg/external/MCGenerators/lhapdf/5.8.9/x86_64-slc6-gcc46-opt/bin/lhapdf-config"
lhapdf_path = os.path.join(MG5_path, "HEPTools/lhapdf6/bin/lhapdf-config")
