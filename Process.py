import math
import dataset_numbers
import subprocess
import config
import os

logger = config.getLogger('2HDM_ttbarSI.Process')

def w(mediator, m_mediator, tanb, MT = 172.5):
    for l in subprocess.check_output([os.path.join(config.root_path, "core", "2HDMC-1.7.0", "CalcPhysGamma"), str(m_mediator), str(tanb), str(MT)], stderr = subprocess.STDOUT).splitlines():
        if l.startswith("w_" + mediator):
            return float(l.split(": ")[1])    

def get_width(mediator, m_mediator, tanb):
    print "Getting width of {mediator} from the thdm ntup. May take some time and please wait...".format(mediator = mediator)
    import ROOT
    f = ROOT.TFile(os.path.join(config.root_path, "core", "thdm_grid_v164.root"))
    thdm = f.thdm
    for i in xrange(thdm.GetEntries()):
        thdm.GetEntry(i)
        if thdm.type == 2:
            if (thdm.sba == 1.) and (thdm.tanb == tanb) and (getattr(thdm, "m" + mediator) == m_mediator):
                return getattr(thdm, "width_" + mediator)
    raise RuntimeError()


class ttbarResonanceProcess(object):
    MT = 172.5
    MB = 4.7
    MTA = 1.777
    sba = 1.
    MC = 1.42
    MM = 0.10566
    HARD = False
    FOUR_FLAVOURS_SCHEME = False
    model = ("Higgs_Effective_Couplings_FormFact","Higgs_Effective_Couplings_FormFact")
    def __init__(self, tanb, signal_type, mA = None, mH = None, wA = None, wH = None, mediator = None, mass = None, reweighted_from = None, sim_config = {}):
        if mediator == "A":
            mA = mass
        elif mediator == "H":
            mH = mass
        if bool(mA) == bool(mH):
            raise TypeError
        self.tanb = tanb
        if mA != None:
            self.mediator = "A"
            self.mA = mA
            self._wA = wA
            self.mH = 1e4
            self.wH = 5.753088e-03
        elif mH != None:
            self.mediator = "H"
            self.mH = mH
            self._wH = wH
            self.wA = 5.753088e-03
            self.mA = 1e4
        self.ymb = None
        self.signal_type = signal_type
        self.reweighted_from = reweighted_from
        self.conventional = False
        self.sim_config = dict(dict(coupling_order = "QED=99 QCD=99", extra_process = ""), **sim_config)
        self._process = None
    def m(self):
        if self.mediator == "A":
            return self.mA
        elif self.mediator == "H":
            return self.mH
        else:
            return self.mA

    @property
    def mass(self):
        return self.m()

    @property
    def wA(self):
        if self._wA == None:
            self._wA = dataset_numbers.get_width("A", self.mA, self.tanb) if self.HARD else w('A', self.mA, self.tanb, self.MT)
        return self._wA
    @wA.setter
    def wA(self, value):
        self._wA = value

    @property
    def WH1(self):
        return self.wA

    @property
    def wH(self):
        if self._wH == None:
            self._wH = dataset_numbers.get_width("H", self.mH, self.tanb) if self.HARD else w('H', self.mH, self.tanb, self.MT)
        return self._wH
    @wH.setter
    def wH(self, value):
        self._wH = value

    @property
    def WH(self):
        return self.wH

    @property
    def width(self):
        if self.mediator == "A":
            return self.wA
        if self.mediator == "H":
            return self.wH
    
    @property
    def ymt(self):
        """ggA/H yukawa coupling constant
        
        Here ggA/H = `ymt`
        Note that in many cases `ymt` is simply yukawa coefficient and the defintion is different.
        """
        if self.mediator == "A":
            return 1./self.tanb * self.MT
        if self.mediator == "H":
            return (math.cos(math.asin(self.sba)) - self.sba/self.tanb) * self.MT
    @property
    def ymc(self):
        if self.mediator == "A":
            return 1./self.tanb * self.MC
        if self.mediator == "H":
            return (math.cos(math.asin(self.sba)) - self.sba/self.tanb) * self.MC            
    @property
    def ymb(self):
        return self._ymb

    @ymb.setter
    def ymb(self, value):
        if value is not None:
            self._ymb = value
            return
        if self.mediator == "A":
            self._ymb = self.tanb * self.MB
        if self.mediator == "H":
            self._ymb = (math.cos(math.asin(self.sba)) + self.sba*self.tanb) * self.MB

    @property
    def ymtau(self):
        if self.mediator == "A":
            return self.tanb * self.MTA
        if self.mediator == "H":
            return (math.cos(math.asin(self.sba)) + self.sba*self.tanb) * self.MTA
    @property
    def ymm(self):
        if self.mediator == "A":
            return self.tanb * self.MM
        if self.mediator == "H":
            return (math.cos(math.asin(self.sba)) + self.sba*self.tanb) * self.MM
    @property
    def MP(self):
        return self.mA
    @property
    def MH(self):
        return self.mH
    @property
    def process(self):
        if self._process is not None:
            return self._process
        if self.signal_type in ("SI", "F"):
            if self.mediator == "A":
                self._process = "g g > t t~ / h"
                return self._process
            if self.mediator == "H":
                self._process = "g g > t t~ / h1"
                return self._process
        if self.signal_type == "S":
            if self.mediator == "A":
                self._process = "g g > h1 > t t~"
                return self._process
            if self.mediator == "H":
                self._process = "g g > h > t t~"
                return self._process
        if self.signal_type == "B":
            self._process = "g g > t t~ / h h1"
            return self._process
    @process.setter
    def process(self, value):
        self._process = value
    @property
    def dataset_number(self):
        if hasattr(self, "_dataset_number"):
            return self._dataset_number
        try:
            self._dataset_number = self.search(zip(*self.parameters)[1], "parameters", ["datasetNumber"])[0]
        except RuntimeError:
            logger.warn('Create a test-only run number: 909090')
            self._dataset_number = 909090
        return self._dataset_number
    @property
    def run_number(self):
        raise DeprecationWarning("Deprecated in the future")
        return self.dataset_number
    def __str__(self, reweighting_info = False, conventional = None):
        if conventional is None:
            conventional = self.conventional
        if not conventional:
            try:
                s = self.search(self.dataset_number, "datasetNumber", ["logicalDatasetName"])[0]
                if reweighting_info and (self.reweighted_from != None):
                    s = s.replace(".{}.".format(self.dataset_number), ".{}from{}.".format(self.dataset_number, self.reweighted_from.dataset_number))
                return s
            except RuntimeError:
                logger.warn("RunNumber is not given. Use conventional name.")
                conventional = True
        if conventional:
            def str_format(a_list):
                for k, v in a_list:
                    if type(v) == str:
                        if k == 'signal_type':
                            v = '1' if v == 'SI' else '0'
                        yield k, v
                        continue
                    if any(prefix in k for prefix in ['cos', 'sin', 'tan']):
                        v *= 100
                        yield k, "{:03d}".format(int(v))
                    else:
                        yield k, str(v)
            
            parameters = str_format(self.parameters)
            s = ''
            i = 0
            if reweighting_info and (self.reweighted_from != None):
                for (_, v1), (_, v2) in zip(parameters, str_format(self.reweighted_from.parameters)):
                    s += v1
                    if v1 != v2:
                        s += 'from' + v2
                    i += 1
                    if i==4:
                        s += '_'
            else:
                for (_, v1) in parameters:
                    s += v1
                    i += 1
                    if i==4:
                        s += '_'
        return s
    @classmethod
    def from_run_number(cls, run_number, reweighted_from = None):
        return cls.from_dataset_number(dataset_number = run_number, reweighted_from = reweighted_from)
    @classmethod
    def from_dataset_number(cls, dataset_number, reweighted_from = None):
        if type(dataset_number) == str:
            dataset_numbers = map(int, dataset_number.split("to")[::-1]) if "to" in dataset_number else map(int, dataset_number.split("from"))
            if reweighted_from:
                dataset_numbers[1:] = [reweighted_from]
            return cls.from_dataset_number(*dataset_numbers)
        mediator, mass, tanb, signal_type = cls.search(dataset_number, "datasetNumber", ["parameters"])[0]
        if type(reweighted_from) in (int, str):
            reweighted_from = cls.from_dataset_number(int(reweighted_from))
        return cls(mediator = mediator, mass = mass, tanb = tanb, signal_type = signal_type, reweighted_from = reweighted_from)
    @property
    def parameters(self):
        return [("mediator", self.mediator), ("mass", self.mass), ("tanb", self.tanb), ("signal_type", self.signal_type)]
    @classmethod
    def search(cls, *names):
        return dataset_numbers.search(*names)
    def _labellist(self, generator = ""):
        keys = dict(interf = "+Interf" if self.signal_type == "SI" else "",
                    generator = generator, 
                    **dict((k, "{} to {}".format(dict(self.reweighted_from.parameters)[k], v) if ((self.reweighted_from != None) and (v != dict(self.reweighted_from.parameters)[k])) else v) for k,v in self.parameters))
        label_pattern = ["{mass}GeV", "{mediator}#rightarrowt#bar{{t}}{interf}", "{generator}", "tan#beta={tanb}"]
        return [l.format(**keys) for l in label_pattern]
    def get_label(self, generator = ""):
        return " ".join(filter(bool, self._labellist(generator)))
    def __repr__(self):
        return self.get_label().replace("#rightarrow", "->") + ' (2HDM)'
    def write_param_card(self, filename = "/dev/stdout"):
        from models.Higgs_Effective_Couplings_FormFact import parameters, write_param_card
        list_of_parameters = []
        for param in parameters.all_parameters:
            if param.nature == "external":
                param.value = getattr(self, param.name, param.value)
                list_of_parameters.append(param)
        write_param_card.ParamCardWriter(filename, list_of_parameters, generic = True)

    def dxsds(self, s):
        Gf = 1.166390e-05
        aS = 1.180000e-01
        cf = [self.ymb/self.MB, self.ymt/self.MT]
        tauf = [(s**2 - 4*self.MB**2)/16/self.MB, (s**2 - 4*self.MT**2)/16/self.MT]
        beta = (1 - tauf[1]**-1)**0.5
        kernal =  sum(map(lambda cf,tauf: cf*self.f(tauf)/tauf/(s - self.mA**2 + 1j*self.mA*self.wA), cf, tauf))
        signal = 3*(aS*Gf*cf[1]*self.MT*s)**2/2048/math.pi**3 * beta * (kernal * kernal.conjugate()).real**0.5
        interf = - (aS*self.MT)**2*cf[1]*Gf/(64*2**0.5*math.pi) * math.log((1+beta)/(1-beta)) * kernal.real
        return signal, interf

    def f(self, tau):
        if tau <= 1:
            return math.asin(tau**0.5)**2
        else:
            return -0.25 * (math.log((1+(1-tau**-1)**0.5)/(1-(1-tau**-1)**0.5)) - math.pi * 1j)**2

class THDMaTtRes(ttbarResonanceProcess):
    model = ("Pseudoscalar_2HDM","Pseudoscalar_2HDM")
    laP1 = 3
    laP2 = 3
    lam3 = 3
    FOUR_FLAVOURS_SCHEME = True # Seems to be neccessary for Pseudoscalar_2HDM_v2 UFO
    def __init__(self, tanb, signal_type, ma, MXd, sinp, mA = None, mH = None, mhc = None, wA = None, wH = None, wa = None, whc = None, mediator = '', mass = None, reweighted_from = None, sim_config = {}):
        self.mediator = mediator
        if not self.mediator:
            if mA != None:
                self.mediator += "A"
                mass = mA
            if mH != None:
                self.mediator += "H"
                mass = mH
            if ma != None and ('A' in self.mediator or self.mediator == '') and 'a' not in self.mediator:
                self.mediator += 'a'
        self._wA = wA
        self._wH = wH
        self._wh = 'auto'
        
        self.ma = ma
        self._wa = wa
        self._whc = whc
        self.MXd = MXd
        self.mA = mA or mass 
        self.mH = mH or mass
        self.mh1 = 125
        self.mh2 = self.mH
        self.mh3 = self.mA
        self.mh4 = ma
        self.mhc = mhc or mass
        self.tanb = tanb
        self.sinp = sinp
        self.ymb = None
        self.signal_type = signal_type
        self.reweighted_from = reweighted_from
        self.conventional = True
        self.sim_config = dict(dict(coupling_order = '[QCD]', extra_process = "", bkg_removal = False), **sim_config)
        self._process = None
    @property
    def wA(self):
        return self._wA or 'auto'
    @property
    def wH(self):
        return self._wH or 'auto'
    @property
    def wHc(self):
        return self._whc or 'auto'
    @property
    def wa(self):
        return self._wa or 'auto'
    @property
    def wh(self):
        return self._wh
    @wh.setter
    def wh(self, value):
        self._wh = value
    @property
    def Wh1(self):
        return self.wh
    @Wh1.setter
    def Wh1(self, value):
        self._wh = value
    @property
    def Wh2(self):
        return self.wH
    @Wh2.setter
    def Wh2(self, value):
        self._wH = value
    @property
    def Wh3(self):
        return self.wA
    @Wh3.setter
    def Wh3(self, value):
        self._wA = value
    @property
    def Wh4(self):
        return self.wa
    @Wh4.setter
    def Wh4(self, value):
        self._wa = value
    @property
    def whc(self):
        return self.wHc
    @whc.setter
    def whc(self, value):
        self._whc = value
    @property
    def tanbeta(self):
        return self.tanb
    @property
    def ymt(self):
        return self.MT#abs(super(THDMaTtRes, self).ymt)
    @property
    def ymb(self):
        return self._ymb
    @ymb.setter
    def ymb(self, value):
        if value is not None:
            self._ymb = value
            return
        self.ymb = self.MB#abs(super(THDMaTtRes, self).ymb)
    @property
    def ymc(self):
        return self.MC
    @property
    def ymm(self):
        return self.MM
    @property
    def ymtau(self):
        return self.MTA#abs(super(THDMaTtRes, self).ymtau)    
    def write_param_card(self, filename = '/dev/stdout', inplace = True):
        temp_filename = '/tmp/param_card.dat'
        from models.Pseudoscalar_2HDM import parameters, write_param_card
        list_of_parameters = []
        auto_computed_parameters = {} 
        for param in parameters.all_parameters:
            if param.nature == "external":
                v = getattr(self, param.name, param.value)
                if v != 'auto':
                    if self.FOUR_FLAVOURS_SCHEME:
                        if param.lhablock in ('YUKAWA', 'MASS') and (param.lhacode[0] <= 4 or param.lhacode[0] in (11,13)):
                            v = 0.
                    param.value = v
                else:
                    param.value = 0.
                    auto_computed_parameters[str(param.lhacode[0])] = param.name
                list_of_parameters.append(param)
        if auto_computed_parameters != []:
            logger.info('Computing widths from MadGraph. It takes roughly 5 minutes and please wait...')
            write_param_card.ParamCardWriter(temp_filename, list_of_parameters, generic = True)
        else:
            write_param_card.ParamCardWriter(filename, list_of_parameters, generic = True)
            return
        compute_widths = '/tmp/compute_widths.mg5'
        stdout = False
        if filename == '/dev/stdout': # MG5 block directly writing into named pipe for some reason
            stdout = True
        with open(os.path.join(config.root_path, 'templates', 'compute_widths.mg5'), 'r') as rf:
            with open(compute_widths, 'w') as wf:
                wf.write(rf.read().format(p = self, particles = ' '.join(auto_computed_parameters.keys()), param_card = temp_filename, output_param_card = filename if not stdout else temp_filename))
        subprocess.check_output([os.path.join(config.MG5_path, 'bin', 'mg5_aMC'), compute_widths])
        if stdout:
            with open(temp_filename) as rf:
                print rf.read()
            if not inplace:
                return
            param_card = temp_filename
        else:
            if not inplace:
                return
            param_card = filename
        self.read_param_card(param_card, blocks = auto_computed_parameters)

    def read_param_card(self, param_card, blocks = None):
        from models.check_param_card import ParamCard
        param_card = ParamCard(param_card)
        if blocks is None:
            from models.Pseudoscalar_2HDM import parameters
            blocks = {} 
            for param in parameters.all_parameters:
                if param.nature == "external":
                    v = getattr(self, param.name, param.value)
                    if v == 'auto':
                        blocks[str(param.lhacode[0])] = param.name
        for p in param_card['decay']:
            lhacode = str(p.lhacode[0])
            if lhacode in blocks:
                setattr(self, blocks[lhacode], p.value)
    @property
    def process(self):
        if self._process is not None:
            return self._process
        elif self.signal_type in ("SI", "F"):
            if self.mediator == "A":
                self._process = "g g > t t~ / h1 h2 h4"
            elif self.mediator == "H":
                self._process = "g g > t t~ / h1 h3 h4"
            elif self.mediator == 'a':
                self._process = "g g > t t~ / h1 h2 h3"
            elif 'a' in self.mediator and 'A' in self.mediator:
                self._process = "g g > t t~ / h1 h2"
        elif self.signal_type == "S":
            if self.mediator == "A":
                self._process = "g g > h3 > t t~"
            elif self.mediator == "H":
                self._process = "g g > h2 > t t~"
            elif self.mediator == 'a':
                self._process = "g g > h4 > t t~ "
            elif 'a' in self.mediator and 'A' in self.mediator:
                self._process = "g g > h3,h4 > t t~"
        elif self.signal_type == "B":
            raise NotImplementedError
            self._process = "g g > t t~"
        return self._process
    @process.setter
    def process(self, value):
        self._process = value
    @property
    def parameters(self):
        return [("mediator", self.mediator), ("mass", self.mass), ("tanb", self.tanb), ("signal_type", self.signal_type), ("mass_a", self.mh4), ("sinp", self.sinp)]
    def __repr__(self):
        return self.get_label().replace("#rightarrow", "->") + " (2HDM+a)"
    def _labellist(self, generator = ""):
        keys = dict(interf = "+Interf" if self.signal_type == "SI" else "",
                    generator = generator,
                    **dict((k, "{} to {}".format(dict(self.reweighted_from.parameters)[k], v) if ((self.reweighted_from != None) and (v != dict(self.reweighted_from.parameters)[k])) else v) for k,v in self.parameters))
        label_pattern = ["{mass}GeV", "{mediator}#rightarrowt#bar{{t}}{interf}", "{generator}", "tan#beta={tanb}"]
        if self.mediator == 'a':
            label_pattern[0] = "{mass_a}GeV"
        return [l.format(**keys) for l in label_pattern]

class THDMaTtRes_FormFac(THDMaTtRes):
    model = ("Pseudoscalar_2HDM_FormFactor", "Pseudoscalar_2HDM")
    def __init__(self, *args, **kwds):
        super(THDMaTtRes_FormFac, self).__init__(*args, **kwds)
        self.sim_config['coupling_order'] = "HIG^2>0"
        self.sim_config['bkg_removal'] = False
        if self.signal_type == 'SI':
            self.sim_config['extra_process'] = "\n".join(['generate {} HIG^2==2 @1'.format(self.process),
                                                          'add process {} HIG^2==1 @2'.format(self.process)])

class THDM_noBKGRemoval(ttbarResonanceProcess):
    MC = 0
    MM = 0
    def __init__(self, *args, **kwds):
        super(THDM_noBKGRemoval, self).__init__(*args, **kwds)
        self.sim_config['coupling_order'] = "HIG^2>0"
        self.sim_config['bkg_removal'] = False
        self.sim_config['extra_process'] = "\n".join(['generate {} HIG^2==2 @1'.format(self.process),
                                                      'add process {} HIG^2==1 @2'.format(self.process)])